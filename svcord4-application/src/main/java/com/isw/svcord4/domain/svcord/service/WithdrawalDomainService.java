package com.isw.svcord4.domain.svcord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord4.sdk.domain.svcord.entity.CreateServicingOrderProducerInput;
import com.isw.svcord4.sdk.domain.svcord.entity.CustomerReferenceEntity;
import com.isw.svcord4.sdk.domain.svcord.entity.Svcord4;
import com.isw.svcord4.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput;
import com.isw.svcord4.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord4.sdk.domain.svcord.service.WithdrawalDomainServiceBase;
import com.isw.svcord4.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord4.sdk.integration.facade.IntegrationEntityBuilder;
import com.isw.svcord4.sdk.integration.partylife.entity.RetrieveLoginInput;
import com.isw.svcord4.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord4.sdk.integration.paymord.entity.PaymentOrderInput;
import com.isw.svcord4.sdk.integration.paymord.entity.PaymentOrderOutput;
import com.isw.svcord4.domain.svcord.command.Svcord4Command;
import com.isw.svcord4.integration.partylife.service.RetrieveLogin;
import com.isw.svcord4.integration.paymord.service.PaymentOrder;
import com.isw.svcord4.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord4.sdk.domain.facade.Repository;

@Service
public class WithdrawalDomainService extends WithdrawalDomainServiceBase {

  private static final Logger log = LoggerFactory.getLogger(WithdrawalDomainService.class);

  public WithdrawalDomainService(DomainEntityBuilder entityBuilder,  Repository repo) {
  
    super(entityBuilder,  repo);
    
  }
  
  @Autowired
  Svcord4Command servicingOrderProcedureCommand;

  @Autowired
  PaymentOrder paymentOrder;

  @Autowired
  RetrieveLogin retrieveLogin;

  @Autowired
  IntegrationEntityBuilder integrationEntityBuilder;

  
  @NewSpan
  @Override
  public com.isw.svcord4.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput execute(com.isw.svcord4.sdk.domain.svcord.entity.WithdrawalDomainServiceInput withdrawalDomainServiceInput)  {

    log.info("WithdrawalDomainService.execute()");
    // TODO: Add your service implementation logic
   
    RetrieveLoginInput retrieveInput = integrationEntityBuilder
    .getPartylife()
    .getRetrieveLoginInput()
    .setId("test1")
    .build();
  
    RetrieveLoginOutput retriveSerivceOutput = retrieveLogin.execute(retrieveInput);
    
    
    if(retriveSerivceOutput.getResult() != "SUCCESS") {
      return null;
    }
  

    CustomerReferenceEntity customerReference = this.entityBuilder
    .getSvcord()
    .getCustomerReference().build();
  
    customerReference.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    customerReference.setAmount(withdrawalDomainServiceInput.getAmount());
  
    CreateServicingOrderProducerInput createInput = this.entityBuilder.getSvcord()
    .getCreateServicingOrderProducerInput().build();
  
    createInput.setCustomerReference(customerReference);
    Svcord4 createOutput = servicingOrderProcedureCommand.createServicingOrderProducer(createInput);

    PaymentOrderInput paymentOrderInput = integrationEntityBuilder.getPaymord().getPaymentOrderInput().build();
    paymentOrderInput.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    paymentOrderInput.setAmount(withdrawalDomainServiceInput.getAmount());
    paymentOrderInput.setExternalId("SVCORD19");
    paymentOrderInput.setExternalSerive("SVCORD19");
    paymentOrderInput.setPaymentType("CASH_WITHDRAWAL");
  
    PaymentOrderOutput paymentOrderOutput = paymentOrder.execute(paymentOrderInput);

    
    
    UpdateServicingOrderProducerInput updateServicingOrderProducerInput = this.entityBuilder.getSvcord()
    .getUpdateServicingOrderProducerInput().build();
    updateServicingOrderProducerInput.setUpdateID(createOutput.getId().toString());
    updateServicingOrderProducerInput
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()));
     

    servicingOrderProcedureCommand.updateServicingOrderProducer(createOutput, updateServicingOrderProducerInput);
  
    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = this.entityBuilder.getSvcord().getWithdrawalDomainServiceOutput()
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()))
    .setTrasactionId(paymentOrderOutput.getTransactionId())
    .build();
  
    return withdrawalDomainServiceOutput;

    
    
    // return null;
  }

}
