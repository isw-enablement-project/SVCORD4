package com.isw.svcord4.domain.svcord.command;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.isw.svcord4.sdk.domain.svcord.command.Svcord4CommandBase;
import com.isw.svcord4.sdk.domain.svcord.entity.Svcord4Entity;
import com.isw.svcord4.sdk.domain.svcord.entity.ThirdPartyReferenceEntity;
import com.isw.svcord4.sdk.domain.svcord.type.ServicingOrderType;
import com.isw.svcord4.sdk.domain.svcord.type.ServicingOrderWorkProduct;
import com.isw.svcord4.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord4.domain.svcord.service.WithdrawalDomainService;
import com.isw.svcord4.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord4.sdk.domain.facade.Repository;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class Svcord4Command extends Svcord4CommandBase {

  private static final Logger log = LoggerFactory.getLogger(Svcord4Command.class);

  public Svcord4Command(DomainEntityBuilder entityBuilder,  Repository repo ) {
    super(entityBuilder,  repo );
  }
    
  @Override
  public com.isw.svcord4.sdk.domain.svcord.entity.Svcord4 createServicingOrderProducer(com.isw.svcord4.sdk.domain.svcord.entity.CreateServicingOrderProducerInput createServicingOrderProducerInput)  {
  

    log.info("Svcord4Command.createServicingOrderProducer()");
    // TODO: Add your command implementation logic

    Svcord4Entity  svcord4Entity =this.entityBuilder.getSvcord().getSvcord4().build();
    svcord4Entity.setCustomerReference(createServicingOrderProducerInput.getCustomerReference());
    // svcord4Entity.setProcessEndDate(LocalDate.now());

    svcord4Entity.setProcessStartDate(LocalDate.now());
    svcord4Entity.setServicingOrderType(ServicingOrderType.PAYMENT_CASH_WITHDRAWALS);
    svcord4Entity.setServicingOrderWorkDescription("CASH WITHDRAWALS");

    svcord4Entity.setServicingOrderWorkProduct(ServicingOrderWorkProduct.PAYMENT);
    
    svcord4Entity.setServicingOrderWorkResult(ServicingOrderWorkResult.PROCESSING);


    ThirdPartyReferenceEntity thirdPartyReference = new ThirdPartyReferenceEntity();
   thirdPartyReference.setId("test1");
   thirdPartyReference.setPassword("password");
   svcord4Entity.setThirdPartyReference(thirdPartyReference);

   return repo.getSvcord().getSvcord4().save(svcord4Entity);

   
    // return null;
  }

  
  @Override
  public void updateServicingOrderProducer(com.isw.svcord4.sdk.domain.svcord.entity.Svcord4 instance, com.isw.svcord4.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput updateServicingOrderProducerInput)  { 

    log.info("Svcord4Command.updateServicingOrderProducer()");
    // TODO: Add your command implementation logic
    
    Svcord4Entity servicingOrderProcedure = this.repo.getSvcord().getSvcord4().getReferenceById(Long.parseLong(updateServicingOrderProducerInput.getUpdateID()));
    servicingOrderProcedure.setCustomerReference(updateServicingOrderProducerInput.getCustomerReference());
    servicingOrderProcedure.setProcessEndDate(LocalDate.now());
    servicingOrderProcedure.setServicingOrderWorkResult(updateServicingOrderProducerInput.getServicingOrderWorkResult());
    servicingOrderProcedure.setThirdPartyReference(updateServicingOrderProducerInput.getThirdPartyReference());
  
    log.info(updateServicingOrderProducerInput.getUpdateID().toString());
    log.info(updateServicingOrderProducerInput.getCustomerReference().getAccountNumber());
    log.info(updateServicingOrderProducerInput.getCustomerReference().getAmount());
  
    this.repo.getSvcord().getSvcord4().save(servicingOrderProcedure);
  

  }
  
}
